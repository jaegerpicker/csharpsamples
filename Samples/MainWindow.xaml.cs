﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;
using System.Collections.Generic;
using System.Windows.Forms;
namespace Samples
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private async void btnFileNumberSort_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!ConsoleManager.HasConsole)
                {
                    ConsoleManager.Show();
                }
                String filename = "";
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                dlg.DefaultExt = ".txt";
                dlg.Filter = "Text Documents (.txt)|*.txt";
                Nullable<bool> result = dlg.ShowDialog();
                if (result == true)
                {
                    filename = dlg.FileName;
                }
                else
                {
                    throw new FileNotFoundException();
                }
                using (StreamReader sr = new StreamReader(filename))
                {
                    String line = await sr.ReadToEndAsync();
                    var numbers = line.Split(',');
                    int[] nums = new int[numbers.Length];
                    int i = 0;
                    foreach (String n in numbers)
                    {
                        int currentNum = 0;
                        if (int.TryParse(n, out currentNum))
                        {
                            nums.SetValue(currentNum, i);
                            i++;
                        }
                    }
                    Array.Sort(nums);
                    Console.WriteLine("Highest Number: " + nums.Max().ToString());
                    Console.WriteLine("Lowest Number: " + nums.Min().ToString());
                    Console.WriteLine("Mean: " + nums.Average().ToString());
                    Console.WriteLine("Median: " + ((nums.Length % 2 != 0) ? (decimal)nums[nums.Length / 2] : ((decimal)nums[nums.Length / 2] + (decimal)nums[nums.Length / 2 - 1]) / 2).ToString());
                }
            }
            catch (EndOfStreamException eos)
            {
                Console.WriteLine("End of the sample file");
            }
            catch (FileNotFoundException fnfe)
            {
                Console.WriteLine("Could Not find the sample text file");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Received a general exception: " + ex.ToString());
            }
            finally
            {
                
            }
        }

        private void btnRomanNumerals_Click(object sender, RoutedEventArgs e)
        {
            if (!ConsoleManager.HasConsole)
            {
                ConsoleManager.Show();
            }
            Random rand = new Random();
            int num = rand.Next(0, 3999);
            String numerals = NumberToRoman(num);
            String number = RomanToNumber(numerals);
            Console.WriteLine("Number(" + num.ToString() + ") to Roman Numeral: " + numerals);
            Console.WriteLine("Numeral(" + numerals + ") to number: " + number);
        }

        private void btnTextFinder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!ConsoleManager.HasConsole)
                {
                    ConsoleManager.Show();
                }
                FolderBrowserDialog dlg = new FolderBrowserDialog();
                DialogResult r = dlg.ShowDialog();
                if (r == System.Windows.Forms.DialogResult.OK)
                {
                    String folderName = dlg.SelectedPath;
                    String search = "*.txt";
                    if (txtSearchString.Text != "")
                    {
                        search = txtSearchString.Text;
                    }
                    string[] files = Directory.GetFiles(folderName, search, SearchOption.AllDirectories);
                    foreach (string f in files)
                    {
                        Console.WriteLine(f);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Execption encountered: " + ex.ToString());
            }
        }

        public String RomanToNumber(String numerals)
        {
            Dictionary<String, int> RomanNumbers = new Dictionary<String, int>();
            RomanNumbers.Add("M", 1000);
            RomanNumbers.Add("CM", 900);
            RomanNumbers.Add("D", 500);
            RomanNumbers.Add("CD", 400);
            RomanNumbers.Add("C", 100);
            RomanNumbers.Add("XC", 90);
            RomanNumbers.Add("L", 50);
            RomanNumbers.Add("XL", 40);
            RomanNumbers.Add("X", 10);
            RomanNumbers.Add("IX", 9);
            RomanNumbers.Add("V", 5);
            RomanNumbers.Add("IV", 4);
            RomanNumbers.Add("I", 1);
            int result = 0;


            foreach (KeyValuePair<String, int> pair in RomanNumbers)
            {
                while (numerals.IndexOf(pair.Key.ToString()) == 0)
                {
                    result += int.Parse(pair.Value.ToString());
                    numerals = numerals.Substring(pair.Key.ToString().Length);
                }
            }
            return result.ToString();

        }

        public string NumberToRoman(int number)
        {
            if (number < 0 || number > 3999)
            {
                throw new ArgumentException("Value must be in the range 0 – 3,999.");
            }
            if (number == 0) return "N";

            int[] values = new int[] { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };
            string[] numerals = new string[] { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };

            StringBuilder result = new StringBuilder();
            for (int i = 0; i < 13; i++)
            {
                while (number >= values[i])
                {
                    number -= values[i];
                    result.Append(numerals[i]);
                }

            }

            return result.ToString();

        }
    }
}
